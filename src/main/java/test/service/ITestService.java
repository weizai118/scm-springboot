package test.service;


/**
 * 标签
 * 
 * @author zhys513
 */
public interface ITestService {
	
	 String test();

	 /**
	  * 测试按命令空间和指定规则kEY缓存
	 * @param str1
	 * @param str2
	 * @return
	 */
	String test(String str1, String str2);
	 
	 /**
	  * 只按命名空间自动生成KEY缓存
	 * @param version
	 * @return
	 */
	String version(String version);

	/**
	 * 按指定命名空间删除缓存
	 * @param version
	 * @return
	 */
	String clearCache(String version);
}
